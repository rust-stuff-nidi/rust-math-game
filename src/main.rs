use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn main() {
    const TOTAL_QUESTIONS: u8 = 10;
    let mut score: u8 = 0;
    println!("\nMATH GAME\n");

    for n in 1..TOTAL_QUESTIONS + 1 {
        let first_number = rand::thread_rng().gen_range(1..=100);
        let second_number = rand::thread_rng().gen_range(1..=100);
        let solution = first_number + second_number;

        println!("\nQuestion {}.   {} + {} = ", n, first_number, second_number);

        let mut guess = String::new();

        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");

        let casted_guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Incorrect. The answer is {}", solution);
                continue
            },
        };

        match casted_guess.cmp(&solution) {
            Ordering::Equal => {
                println!("Correct");
                score += 1;
            }
            _ => {
                println!("Incorrect. The answer is {}", solution);
            }
        }
    }

    println!("\nFinal score: {} of {}", score, TOTAL_QUESTIONS);
}
